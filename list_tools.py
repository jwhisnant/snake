#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: list_tools.py
Author: jwhisnant
Email: jwhisnant@email.com
bitbucket: https://bitbucket.org/jwhisnant
Description:
"""

import random
import time

from tqdm import trange
from profilehooks import profile

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@profile(filename='/var/tmp/rand_list.prof', entries=100, stdout=False)
def rand_list(size):
    """ make random list of a size """
    alist = []
    # for x in range(size):
    for x in trange(size):
        alist.append(x)

    random.shuffle(alist)
    return alist


@profile(filename='/var/tmp/pop_left.prof', entries=100, stdout=False)
def pop_left(alist):
    now = time.time()
    logger.info('pop left start %s' % (time.ctime(now)))
    while alist:
        alist.pop(0)
    logger.info('pop left end, elapsed: %.2f' % (time.time() - now))


@profile(filename='/var/tmp/pop_right.prof', entries=100, stdout=False)
def pop_right(alist):
    now = time.time()
    logger.info('pop left start %s' % (time.ctime(now)))
    logger.info('pop right start')
    while alist:
        alist.pop()
    logger.info('pop right end, elapsed: %.2f' % (time.time() - now))


@profile(filename='/var/tmp/pop_mid.prof', entries=100, stdout=False)
def pop_mid(alist):
    now = time.time()
    logger.info('pop left start %s' % (time.ctime(now)))
    logger.info('pop mid start')
    while alist:
        alist.pop(len(alist) // 2)
    logger.info('pop mid end, elapsed: %.2f' % (time.time() - now))


@profile(filename='/var/tmp/sort.prof', entries=100, stdout=False)
def sort(alist):
    # XXX FIXME: mutable as arg 2018/09/27  (jwhisnant)
    logger.info('sort start')
    alist.sort()
    logger.info('sort end')


@profile(filename='/var/tmp/setup.prof', entries=100, stdout=False)
def setup(size):
    for func in (pop_right, pop_mid, pop_left, sort):
        alist = rand_list(size=size)
        func(alist)


if __name__ == '__main__':
    setup(size=1000 * 100 * 2)
    #setup(size=1000 * 100 * 1)
